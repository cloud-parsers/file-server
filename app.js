var http = require('http'); 
var url = require('url');
var fs = require('fs');
var path = require('path');
var mime = require('mime-types')

try {
  var config = require('./config');
} catch (err) {
  if (err.code === 'MODULE_NOT_FOUND') {
    const jsonBase = {
      "local" : "local",
      "far" : "base",
      "timeout" : 900000
    };
    const jsonBaseString = JSON.stringify(jsonBase);
    fs.writeFile('config.json', jsonBaseString, function (err) {
      if (err) throw err;
      console.log('Config file created.');
    });
  } else {
    console.error(err);
  }
}

var timerMap = new Map();

////////////////////////////////////////////////////////
/////////Функция рекурсивного поиска///////////////////
///////////////////////////////////////////////////////

function searchRecursive (dir, file) {
   var results;

   fs.readdirSync(dir).forEach(function (dirInner) {
     dirInner = path.resolve(dir, dirInner);
     var stat = fs.statSync(dirInner);

    if (stat.isDirectory()) {
       return results = searchRecursive(dirInner, file);
     }
     else if (!fs.existsSync(path.join(dir ,file))) {
       return results = dirInner;
    }
   });

   return results;
};

////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////
////////////////Функціонал сервера///////////////////////
/////////////////////////////////////////////////////////
var server = http.createServer(function (req, res) {

//--------------------------------------------------------
//-------------Отримуєм данні з url---------------------
//--------------------------------------------------------

  var url_parts = url.parse(req.url, true);
  var name = url_parts.query['file']; console.log(name);

  if(name==undefined)
  {
    res.end("<h1>Empety request</h1>");
    return;
  }

//--------------------------------------------------
//----------Пошук і копіювання файла----------------
//--------------------------------------------------
var dir = path.join(config.local, name);

if (!fs.existsSync(dir))
{
  var files = searchRecursive(config.far, name);
  if (fs.existsSync(files))
  {
    fs.copyFileSync(files, dir,fs.constants.COPYFILE_EXCL, (err) => {
      if (err) throw err;
    });

    var timerID = setTimeout(() => {
      fs.unlinkSync(dir)
    }, config.timeout);
    timerMap.set(dir, timerID);
  }

}else {

  var timerID = timerMap.get(dir);
  clearTimeout(timerID);
  timerID = setTimeout(() =>
  {
    fs.unlinkSync(dir)
  }, config.timeout);
  timerMap.set(dir, timerID);
}

//-------------------------------------------------------
//-------------Відпра файла---------------------------
//-------------------------------------------------------

fs.readFile(dir, function(err, content) {
  if (err) {
    res.writeHead(404, { "Content-type": "text/html" });
    res.end("<h1>No such file</h1>");
  } else {
      res.writeHead(200, { 
        "Content-Disposition":filename=name,
        "Content-type": mime.lookup(name)
      });
      res.end(content);
  }
});
//--------------------------------------------------------
});
/////////////////////////////////////////////////////////////////////////////

const port = process.env.PORT || 8080;
server.listen(port, () => {
    console.log(`Server is running. Port:${port}`);
});
